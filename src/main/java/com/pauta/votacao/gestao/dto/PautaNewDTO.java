package com.pauta.votacao.gestao.dto;

import java.io.Serializable;

public class PautaNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String titulo;
	private Boolean sessaoOpen;
	private Integer tempoSessao;

	public Integer getTempoSessao() {
		return tempoSessao;
	}

	public void setTempoSessao(Integer tempoSessao) {
		this.tempoSessao = tempoSessao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Boolean getSessaoOpen() {
		return sessaoOpen;
	}

	public void setSessaoOpen(Boolean sessaoOpen) {
		this.sessaoOpen = sessaoOpen;
	}

	

}
