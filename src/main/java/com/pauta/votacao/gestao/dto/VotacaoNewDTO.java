package com.pauta.votacao.gestao.dto;

import java.io.Serializable;

import com.pauta.votacao.gestao.services.validation.VotacaoInsert;

@VotacaoInsert
public class VotacaoNewDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer pauta_id;

	private String id_associado;

	private Boolean voto;
	
	public VotacaoNewDTO() {
	}

	public Integer getPauta_id() {
		return pauta_id;
	}

	public void setPauta_id(Integer pauta_id) {
		this.pauta_id = pauta_id;
	}

	public String getId_associado() {
		return id_associado;
	}

	public void setId_associado(String id_associado) {
		this.id_associado = id_associado;
	}

	public Boolean getVoto() {
		return voto;
	}

	public void setVoto(Boolean voto) {
		this.voto = voto;
	}
	
	
}
