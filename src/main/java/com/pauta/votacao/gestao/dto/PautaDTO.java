package com.pauta.votacao.gestao.dto;

public class PautaDTO {
	private String titulo;
	private Boolean sessaoOpen;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Boolean getSessaoOpen() {
		return sessaoOpen;
	}

	public void setSessaoOpen(Boolean sessaoOpen) {
		this.sessaoOpen = sessaoOpen;
	}

}
