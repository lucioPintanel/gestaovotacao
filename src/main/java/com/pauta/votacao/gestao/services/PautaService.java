package com.pauta.votacao.gestao.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pauta.votacao.gestao.domain.Pauta;
import com.pauta.votacao.gestao.domain.Votacao;
import com.pauta.votacao.gestao.dto.PautaNewDTO;
import com.pauta.votacao.gestao.repositories.PautaRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class PautaService {

	@Autowired
	private PautaRepository repo;

	public Pauta find(Integer id) throws ObjectNotFoundException {
		Optional<Pauta> obj = repo.findById(id);
		obj.get().setVotoSim(0);
		obj.get().setVotoNao(0);
		obj.get().setTotalVotantes(obj.get().getVotacao().size());
		for (Votacao vot : obj.get().getVotacao()) {
			if (vot.getVoto()) {
				obj.get().setVotoSim(obj.get().getVotoSim() + 1);
			} else {
				obj.get().setVotoNao(obj.get().getVotoNao() + 1);
			}
		}

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Pauta.class.getName()));
	}

	public List<Pauta> findAll() {
		return repo.findAll();
	}

	@Transactional
	public Pauta insert(Pauta obj) {
		obj.setId(null);
		return repo.save(obj);
	}

	public Pauta update(Pauta obj, PautaNewDTO objDto) throws ObjectNotFoundException {
		Pauta newObj = find(obj.getId());
		updateData(newObj, obj, objDto);
		return repo.save(newObj);
	}

	public Pauta fromDTO(PautaNewDTO objDto) throws ObjectNotFoundException {
		Pauta pautanew = new Pauta(null, objDto.getTitulo(), false);
		return pautanew;
	}

	private void updateData(Pauta newObj, Pauta obj, PautaNewDTO objDto) {
		LocalDateTime myDateObj = LocalDateTime.now();
		newObj.setDateInicio(myDateObj);
		long tempo = (objDto.getTempoSessao() != 0) ? objDto.getTempoSessao() : 1;
		newObj.setDateFim(myDateObj.plusMinutes(tempo));
		newObj.setSessaoOpen(true);
	}
}
