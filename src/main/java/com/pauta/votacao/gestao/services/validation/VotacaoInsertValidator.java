package com.pauta.votacao.gestao.services.validation;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.pauta.votacao.gestao.dto.VotacaoNewDTO;
import com.pauta.votacao.gestao.resources.exception.FieldMessage;

public class VotacaoInsertValidator implements ConstraintValidator<VotacaoInsert, VotacaoNewDTO> {

	@Override
	public void initialize(VotacaoInsert ann) {
	}

	@Override
	public boolean isValid(VotacaoNewDTO objDto, ConstraintValidatorContext context) {

		List<FieldMessage> list = new ArrayList<>();

		RestTemplate restTemplate = new RestTemplate();
		// request url
		final String url = "https://user-info.herokuapp.com/users/" + objDto.getId_associado();

		URI uri = null;
		try {
			uri = new URI(url);
			ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
			System.out.println(result.getStatusCodeValue());
			System.out.println(result.getBody().contains("UNABLE"));

			// make a request
			if (result.getStatusCodeValue() != 200) {
				System.out.println("Status code");
				list.add(new FieldMessage("id_associado", "Status code invalido: " + result.getStatusCodeValue()));
			}
			if (result.getBody().contains("UNABLE")) {
				System.out.println("CPF inválido");
				list.add(new FieldMessage("id_associado", "CPF inválido"));
			}

		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (HttpClientErrorException ex) {
			list.add(new FieldMessage("id_associado", "CPF inválido"));
			System.out.println("CPF inválido 123");
		}

		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		
		System.out.println(list.isEmpty());

		return list.isEmpty();
	}

}
