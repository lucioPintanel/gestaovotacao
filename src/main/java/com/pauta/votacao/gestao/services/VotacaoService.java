package com.pauta.votacao.gestao.services;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pauta.votacao.gestao.domain.Pauta;
import com.pauta.votacao.gestao.domain.Votacao;
import com.pauta.votacao.gestao.dto.VotacaoNewDTO;
import com.pauta.votacao.gestao.repositories.PautaRepository;
import com.pauta.votacao.gestao.repositories.VotacaoRepository;

import javassist.tools.rmi.ObjectNotFoundException;

@Service
public class VotacaoService {

	@Autowired
	private VotacaoRepository repo;

	@Autowired
	private PautaRepository pautarepo;

	@Autowired
	private PautaService service;

	public Votacao find(Integer id) throws ObjectNotFoundException {
		Optional<Votacao> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Votacao.class.getName()));
	}

	@Transactional
	public Votacao insert(Votacao obj) {
		obj.setId(null);
		return repo.save(obj);
	}

	public Votacao fromDTO(VotacaoNewDTO objDto) throws ObjectNotFoundException {
		Pauta pauta = service.find(objDto.getPauta_id());
		if (pauta.getSessaoOpen()) {
			LocalDateTime myDateObj = LocalDateTime.now();
			if (myDateObj.isBefore(pauta.getDateFim())) {
				boolean result = false;
				for(Votacao i : pauta.getVotacao()){
		            if(i.getId_associado().equals(objDto.getId_associado())){
		                result = true;
		                break;
		            }
		        }
				if (!result) {
				Votacao votacaonew = new Votacao(null, objDto.getId_associado(), objDto.getVoto());
					votacaonew.setPauta(pauta);
					return votacaonew;
				}
			} else {
				pauta.setSessaoOpen(false);
				pautarepo.saveAll(Arrays.asList(pauta));
			}
		}
		return null;
	}
}
