package com.pauta.votacao.gestao.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pauta.votacao.gestao.domain.Votacao;
import com.pauta.votacao.gestao.dto.VotacaoNewDTO;
import com.pauta.votacao.gestao.services.VotacaoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping(value = "/votacao")
//@Api(value = "Conjunto de endpoints para criar, recuperar e atualizar Votacao.")
@Api(tags = "Votacao")
public class VotacaoResources {

	@Autowired
	private VotacaoService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation("${votacaoresources.find}")
	public ResponseEntity<?> find(
			@ApiParam("Id de votação para obter o voto. Não pode estar vazio.") @PathVariable Integer id)
			throws ObjectNotFoundException {
		Votacao obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation("${votacaoresources.insert}")
	public ResponseEntity<Void> insert(
			@ApiParam("Informações do novo voto para adicionar o voto.") @Valid @RequestBody VotacaoNewDTO objDto)
			throws ObjectNotFoundException {
		Votacao obj = service.fromDTO(objDto);
		if (obj != null) {
			obj = service.insert(obj);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId())
					.toUri();
			return ResponseEntity.created(uri).build();
		}
		return ResponseEntity.notFound().build();
	}
}
