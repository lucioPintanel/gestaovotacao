package com.pauta.votacao.gestao.resources;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.pauta.votacao.gestao.domain.Pauta;
import com.pauta.votacao.gestao.dto.PautaNewDTO;
import com.pauta.votacao.gestao.services.PautaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import javassist.tools.rmi.ObjectNotFoundException;

@RestController
@RequestMapping(value = "/pauta")
//@Api(value = "Conjunto de endpoints para criar, recuperar e atualizar Pauta.")
@Api(tags = "Pauta")
public class PautaResources {

	@Autowired
	private PautaService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ApiOperation("${pautaresources.find}")
	public ResponseEntity<?> find(
			@ApiParam("Id de pauta para obter a pauta. Não pode estar vazio.") @PathVariable Integer id)
			throws ObjectNotFoundException {
		Pauta obj = service.find(id);
		return ResponseEntity.ok().body(obj);

	}

	@RequestMapping(method = RequestMethod.GET)
	@ApiOperation("${pautaresources.find}")
	public ResponseEntity<List<Pauta>> find() throws ObjectNotFoundException {
		List<Pauta> list = service.findAll();
		return ResponseEntity.ok().body(list);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation("${pautaresources.insert}")
	public ResponseEntity<Void> insert(
			@ApiParam("Informações da nova pauta para ser criada.") @RequestBody PautaNewDTO objDto)
			throws ObjectNotFoundException {
		Pauta obj = service.fromDTO(objDto);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@ApiOperation("${pautaresources.update}")
	public ResponseEntity<Void> update(
			@ApiParam("Informações da pauta para fazer alteracao.") @RequestBody PautaNewDTO objDto,
			@PathVariable Integer id) throws ObjectNotFoundException {
		Pauta obj = service.fromDTO(objDto);
		obj.setId(id);
		obj = service.update(obj, objDto);
		return ResponseEntity.noContent().build();
	}

}
