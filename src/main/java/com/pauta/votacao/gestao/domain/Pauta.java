package com.pauta.votacao.gestao.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Pauta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String titulo;

	@Column
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime dateInicio;

	@Column
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private LocalDateTime dateFim;

	@Column
	private Boolean sessaoOpen;

	@JsonManagedReference
	@OneToMany(mappedBy = "pauta", cascade = CascadeType.ALL)
	private List<Votacao> votacao = new ArrayList<>();

	@Column
	private Integer votoSim;

	@Column
	private Integer votoNao;

	@Column
	private Integer totalVotantes;

	public Pauta() {
	}

	public Pauta(Integer id, String titulo, Boolean sessaoOpen) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.sessaoOpen = sessaoOpen;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Votacao> getVotacao() {
		return votacao;
	}

	public void setVotacao(List<Votacao> votacoes) {
		this.votacao = votacoes;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public LocalDateTime getDateInicio() {
		return dateInicio;
	}

	public void setDateInicio(LocalDateTime date) {
		this.dateInicio = date;
	}

	public LocalDateTime getDateFim() {
		return dateFim;
	}

	public void setDateFim(LocalDateTime date) {
		this.dateFim = date;
	}

	public Boolean getSessaoOpen() {
		return sessaoOpen;
	}

	public void setSessaoOpen(Boolean sessaoOpen) {
		this.sessaoOpen = sessaoOpen;
	}

	public Integer getVotoSim() {
		return votoSim;
	}

	public void setVotoSim(Integer votoSim) {
		this.votoSim = votoSim;
	}

	public Integer getVotoNao() {
		return votoNao;
	}

	public void setVotoNao(Integer votoNao) {
		this.votoNao = votoNao;
	}

	public Integer getTotalVotantes() {
		return totalVotantes;
	}

	public void setTotalVotantes(Integer totalVotantes) {
		this.totalVotantes = totalVotantes;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pauta other = (Pauta) obj;
		return Objects.equals(id, other.id);
	}
}
