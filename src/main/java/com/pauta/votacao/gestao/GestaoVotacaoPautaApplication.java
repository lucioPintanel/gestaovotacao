package com.pauta.votacao.gestao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestaoVotacaoPautaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestaoVotacaoPautaApplication.class, args);
	}

}
