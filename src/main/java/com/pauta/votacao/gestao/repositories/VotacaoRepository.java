package com.pauta.votacao.gestao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pauta.votacao.gestao.domain.Votacao;

@Repository
public interface VotacaoRepository extends JpaRepository<Votacao, Integer> {
	
	public Votacao findByAssociado(String associado);
}
