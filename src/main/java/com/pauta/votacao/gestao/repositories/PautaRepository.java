package com.pauta.votacao.gestao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pauta.votacao.gestao.domain.Pauta;

@Repository
public interface PautaRepository extends JpaRepository<Pauta, Integer> {

}
